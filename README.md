# O zadaniu #

Zadanie polega na wykonaniu aplikacji frontendowej za pomocą react i redux.
Która na celu bedzie miała: wyświeltanie wartosci numerycznej + 2 buttony, jeden od zwiekszania watosci o 1 drugi od zmniejszania jej o 1.
Wiem, że o samym react już coś wiecie dlatego skupie sie na redux.

O Redux:
https://www.nafrontendzie.pl/podstawy-redux-zarzadzanie-stanem-react
btw polecam ten blog, nie dość, że w ojczystym języku to dość konkretny :)

### Co należy zrobić ###

* Niech każdy z was (Dominika i Piotrek) zrobi sobie nowy branch w tym repo podpisany imieniem.
* Nastepnie użyjcie https://github.com/facebook/create-react-app do postawienia reacta
* Dodajcie bibloteki redux i react-redux
* Store aplikacji ma przetrzymywać wartość "countera", przy każdym odświeżeniu wartość ma być równa 0
* Zaden komponet react nie ma mieć stojego "state"

### Na koniec ###

* W razie problemów smiało pisać
* Wspołpraca mile widziana ale unikajcie kopiowania swojego kodu